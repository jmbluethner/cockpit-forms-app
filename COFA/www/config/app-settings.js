/**
 * @description This variable defines all the api settings
 * @type {{debugMode: boolean}}
 */
export const applicationSettings = {
    debugMode: true
}