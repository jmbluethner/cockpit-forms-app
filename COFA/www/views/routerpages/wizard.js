export const Wizard = {
    data: function () {
        return {
            wizardStep: 0
        }
    },
    mounted: function () {

    },
    created: function () {

    },
    methods: {
        checkIfConfigExists: function() {
            let storage = window.localStorage;
            if(storage.getItem('cockpit-key')) {
                window.location.href = '/index.html#/overview'
            }
        }
    },
    template: `
        <div data-router-page="wizard">
            <h1>Howdy!</h1>
            <p>It looks like you have opened this app for the first time. That's why you need to tell me some basic things so we can start.</p>
            <form onsubmit="return false">
                <label>Your Cockpit host
                    <input type="text" placeholder="cockpit.example.com">
                </label>
                <label>Cockpit API key
                    <input type="text" placeholder="029fn20newfu9082fh">
                </label>
                <button type="submit">Let's go!</button>
            </form>
        </div>
    `
}