import {applicationSettings} from '../config/app-settings.js';
import {logToConsole} from "../js/logger.js";
import * as importer from '../js/importer.js';


export function prepareVue(callback) {
    // Import vue
    if(applicationSettings.debugMode) {
        // Use the development version of vue
        logToConsole('Loading Vue in Development mode.','info');
        importer.appendJsFile('../lib/vue/vue.js','text/javascript', function () {
            importer.appendJsFile('../lib/vue-router/vue-router.js','text/javascript', function () {
                callback();
            });
        });
    } else {
        // Use the production version of vue
        logToConsole('Loading Vue in production mode. Wait... You shouldn\'t see this! Some dev messed up here ...','info');
        importer.appendJsFile('../lib/vue/vue.min.js','text/javascript',function () {
            importer.appendJsFile('../lib/vue-router/vue-router.js','text/javascript', function () {
                callback();
            });
        });
    }
}