// Import misc JS
import {prepareVue} from "../main.js";
import {logToConsole} from "../../js/logger.js";
import {callApi} from "../../js/api-client.js";

// Import components
import {Wizard} from "../routerpages/wizard.js";
import {Overview} from "../routerpages/overview.js";

prepareVue(function () {

    logToConsole('Initializing Vue views...', 'info');

    let bus = {};
    bus.eventBus = new Vue();

    Vue.use(VueRouter)

    const router = new VueRouter({
        routes: [
            {
                path: '/',
                component: Wizard,
                name: "wizard"
            },
            {
                path: '/overview',
                component: Overview,
                name: "overview"
            }
        ]
    });

    // Create app
    let app = new Vue({

        el: '#app',
        data: function () {
            return {
                user: {}
            }
        },
        components: {

        },
        mounted: function() {

        },
        created: function () {

        },
        computed: {

        },
        methods: {

        },
        router,
        template: `
        `
    });
});