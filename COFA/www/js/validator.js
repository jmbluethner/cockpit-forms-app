import {callApi} from "./api-client.js";

export function validateCockpitToken(token) {
    if(token.length !== 38) {
        return false;
    }
    if(token.startsWith('account-')) {
        return false;
    }
}