/**
 * @author jmbluethner <mail@jmbluethner.de>
 */

// Import requirements
import {logToConsole} from './logger.js';
import * as urlHandler from './url-handler.js';

/**
 * @function callApi()
 * @description A general function to talk to the API
 * @param {string} route The URL for the XHR call
 * @param {string} reqBody Request Body
 * @param {string} method GET,POST,DELETE,PUT
 * @param {string} contentType Request content type
 * @param {function} callback The Callback function gets called when the API responded with Status 200
 * @returns {boolean}
 */
export function callApi(route,reqBody,method,contentType,callback) {
    // Define the route
    let url = route;
    logToConsole('Trying to call the API at '+url,'info');
    let vue = this;
    // Create new request object
    let xhr = new XMLHttpRequest();
    // Detect when the XHR Request got started
    xhr.onreadystatechange = function() {
        if(xhr.readyState === 4 && this.status === 200) {
            // If the XHR Request is ready (4) and the HTTP Status is 200 (OK) ...
            logToConsole('Received data from the API at'+url,'success');
            let resp = JSON.parse(this.response);
            resp.httpResponseCode = this.status;
            callback(resp);
        } else if(xhr.readyState === 4 && this.status !== 200) {
            let httpRsesponseCode = this.status;
            // Else if the HTTP response code wasn't 200 ...
            //console.log(this.status);
            // Check if the token has expired or is invalid in itself
            logToConsole('Received data from the API at '+url+' but the HTTP response Code was '+httpRsesponseCode+'. But that might be fine.','warning');
        }
    };
    // Open the connection
    xhr.open(method, url, true);
    // Set all header
    xhr.setRequestHeader("Content-Type", contentType);
    // Fire the request
    xhr.send(reqBody);
}