/**
 * @function appendCssFile
 * @description Allows you to load CSS whenever you want
 * @param {string} file CSS file, can be local or even global
 * @param {function} callback  The callback function gets called once the process is done
 */
export function appendCssFile(file,callback) {
    let cssFile = document.createElement("link");
    cssFile.rel =  "stylesheet";
    cssFile.href  = file;
    cssFile.type = "text/css";
    document.getElementsByTagName("head")[0].appendChild(cssFile);
    callback();
}

/**
 * @function appendJsFile
 * @description Use this to dynamically load JS (new <script> will be created in <head>)
 * @param {string} file The JS file you want to load
 * @param {function} callback The callback function gets called once the script has loaded
 * @param {string} type Can be used to set the script type, for modules for example
 */
export function appendJsFile(file,type,callback) {
    let script = document.createElement('script');
    script.onload = function() {
        callback();
    }
    script.src = file;
    script.type = type;
    document.head.appendChild(script);
}
