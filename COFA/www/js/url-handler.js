export function getBaseUrl() {
    return window.location.origin;
}

export function getCurrentPage() {
    let base_url = window.location.origin;
    let host = window.location.host;
    let pathArray = window.location.pathname.split( '/' );
    return pathArray[pathArray.length-1];
}